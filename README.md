# ansible-nginx-acme

Installs nginx with a privacy-friendly log config and prepares for lets encrypt
validation with  [acmetool](https://github.com/hlandau/acme).

You should use the [ansible-acmetool](https://gitlab.com/guardianproject-ops/ansible-acmetool) role after this one.

It creates an nginx config at

    /etc/nginx/sites-enables/{{acmetool_domains}}.conf

so you should overwrite this file with your config eventually.

## Requirements

* Debian

## Role Variables

```
nginx_log_rotate_days: 10
```


## Example Playbook

```yaml
- hosts: servers
  vars:
    acmetool_domains: foobar.example.com
  roles:
    - ansible-nginx-acme
    - ansible-acmetool
```

License
-------

[GNU Affero General Public License (AGPL) v3+](https://www.gnu.org/licenses/agpl-3.0.en.html)

Author Information
------------------

```
Abel Luck <abel@guardianproject.info>
Guardian Project https://guardianproject.info
```

